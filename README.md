# CHARIS OAI-PMH to MongoDB importer

This is a Node.js app written in Coffeescript. Dependencies are managed by NPM.


## Set Up Environment
You will need to install Node.js, npm, and coffeescript on your system if you haven't already.

On Mac OSX using homebrew:

    brew install node

On Debian/Ubuntu:

	apt-get install node

To install dependencies for this script, clone this repo and run :

	npm install
in the directory you cloned to.

Before you can run/compile this script install coffeescript if you haven't already

	npm install -g coffeescript


## Compile or Run Script

This script takes 3 variables, the hostname (**$host**) of the server hosting OAI-PMH library, the base path (**$path**) to the OAI-PMH endpoint, and the OAI verb and metadata format (**$parameters**) to be requested.

This script can be run interpreted from coffeescript (better for development and testing):

	coffee oai.coffee $host $path $parameters
Or compiled to pure Javascript and run with node (faster and better for production)

	coffee -c oai.coffee
	node oai.js $host $path $parameters

## Using the default OAI-PMH endpoints
#####To harvest the Digital Commons (BePress) Endpoint:
Use either

	node oai.js 'digitalcommons.acu.edu' '/do/oai/' '?verb=ListRecords&metadataPrefix=oai_dc'
	
or

	coffee oai.coffee 'digitalcommons.acu.edu' '/do/oai/' '?verb=ListRecords&metadataPrefix=oai_dc'

	
#####To harvest the Poral of Texas History (UNT) endpoint
Use either

     coffee oai.coffee 'texashistory.unt.edu' '/explore/partners/ACUL/oai/' '?verb=ListRecords&metadataPrefix=oai_dc'

or

     node oai.js 'texashistory.unt.edu' '/explore/partners/ACUL/oai/' '?verb=ListRecords&metadataPrefix=oai_dc'
 

## Database Integration

Database settings are in the "Configuration" section of the script. Please Change lines 17 and 18 to reflect the location of your database. Currently the script is set up to point to 54.69.153.42, the EC2 instance Polymath set up to host the Mongo Database for this project.