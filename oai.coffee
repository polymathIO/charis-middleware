http = require 'http'
fs = require 'fs'
moment = require 'moment'
compressor = require 'node-minify'
readline = require 'readline'
stream = require 'stream'
xml2js = require 'xml2js'
json = require 'format-json'
MongoClient = require('mongodb').MongoClient
format = require('util').format
datejs = require 'datejs'
sys = require 'sys'

exec = require('child_process').exec

insertToDb = (array) ->
	MongoClient.connect 'mongodb://ec2-54-68-83-204.us-west-2.compute.amazonaws.com:27017/charis', (err, db) ->
	  if err?
	  	throw err
	  console.log 'connected'
	  collection = db.collection 'Metadata'

	  for obj , index in array by -1
		  collection.insert obj.metadata.dc, (err, docs) ->
				if err?
					throw err	
				console.log 'wrote file' + index, obj.metadata.dc
				if index is 0
					db.close (err, res) ->
						if err?
							throw err
							console.log 'closed'
					return
		return
	return

Date.i18n.setLanguage 'en-US'

# server = new mongo.Server "54.69.153.42", 27017, {}
# client = new mongo.Db 'test', server, {w:1}
# client = require("mongojs").connect "mongodb://54.69.153.42:27017/test", ["records"]


basePath = process.argv[3]
params = process.argv[4]

options =
  host: process.argv[2]
  path: basePath + params

#basePath = '/explore/partners/ACUL/oai/'
#params = '?verb=ListRecords&metadataPrefix=oai_dc'

#options =
#  host: "texashistory.unt.edu"
#  path: basePath + params

token = null
fileIndex = 0
tokenLine = null
totalLines = -2 # -1 for indexing and -1 for blank final line

folderName = "records-" + options.host + '-' + moment().format("MM-DD-YY-X")

saveDocs = null
loopIndex = 0


extractToken = (xml) ->
  xml = xml.split '\n'
  totalLines += xml.length
  for line ,index in xml by -1
    loopIndex++
    if /\<resumptionToken/.test line
      fileIndex++
      token = line.split(">")[1].split("<")[0]              
      console.log "resumptionToken = " + token  
      params = "?verb=ListRecords&resumptionToken=" + token
      makeRequest
          host: options.host
          path: basePath + params
      break
    else
      token = null
      if index is 0
        concatResponses folderName
  return

exportRecords = (dirName, xmlData) ->
  fs.mkdirSync "./data/" + dirName  if fileIndex is 0
  fs.writeFile "./data/" + dirName + "/results" + fileIndex, xmlData, (err) ->
    if err
      console.log err
    else
      console.log '\nWrote Partial records file: results' + fileIndex
      extractToken xmlData
    return
  return

makeRequest = (opt) ->
  http.request(opt, processResponse).end()
  return

processResponse = (response) ->
  console.log "Connected to OAI-PMH Server"
  xmlData = ""
  response.on "data", (chunk) ->
    xmlData += chunk
    process.stdout.write '.'
    return 
  response.on "end", ->
      exportRecords folderName, xmlData
    return
  return

removeInteriorDoctypes = (file) ->
  fs.readFile file, (err, data) =>
    if err?
      throw err
    i = 0;
    instream = fs.createReadStream file, { encoding: 'utf8' }
    outstream = new stream;
    rl = readline.createInterface(instream, outstream);
    outputData = ''

    escapeRegExp = (str) ->
      return str.replace /[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&"

    linesToRemove = [
      '<?xml',
      '<XML',
      '<OAI-PMH',
      '</OAI-PMH',
      '<responseDate',
      '<request verb',
      '<ListRecords',
      '</ListRecords',
      '<resumptionToken' 
    ]

    rl.on 'line', (line) ->
      if i < 0 || i > totalLines + 1
        console.log 'Line is within exception range: ' + line
        outputData += line + '\n'      
      else
        testsPassed = 0
        for subString, index in linesToRemove
          regExpSubstring = new RegExp(escapeRegExp subString)
          if regExpSubstring.test line
            #console.log 'Removing Offensive Line (' + subString + ') : ' + line
            break
          else 
            testsPassed++
            # console.log 'Passed ' + testsPassed + ' out of ' + linesToRemove.length + ' tests'
            if testsPassed == linesToRemove.length                  
            	if /\<dc:date\>/.test line
                dateStr = line.split('>')[1].split('<')[0]
              if /</.test line  #hacky to deal with weird formatting from Digital Commons
                outputData += line + '\n'
              # console.log 'Line has passed all tests: ' + line
      i++
      return
    rl.on 'close', () ->
      console.log 'Total Length: ' + totalLines
      wrapData = '<ListRecords>\n' + outputData + '\n</ListRecords>'
      cleanData = wrapData.replace '\ufeff', ''
      fs.writeFile './data/' + folderName + '/recordsToConvert.xml', cleanData, () ->
        console.log 'Wrote Convertable XML file'
        xmlToJson './data/' + folderName + '/recordsToConvert.xml'
        return
      return
    return
  return

writeToMongo = (collection, file) ->
    puts = (error, stdout, stderr) ->
      console.log 'Imported to Mongo'
      #console.log 'stdout: ' + stdout
      console.log 'stderr: ' + stderr
      if error?
        console.log 'exec error: ' + error
      sys.puts stdout
    console.log "mongoimport --file ~/src/charis-middleware/#{file} --collection #{collection} --jsonArray"
    exec "mongoimport --file ~/src/charis-middleware/#{file} --db charis --collection #{collection} --jsonArray", puts

trimLines = (string) ->
    string2 = string.substring(0, string.lastIndexOf("\n"))
    strLines = string2.split '\n'
    strLines.splice 0 , 1
    newtext = strLines.join '\n'
    return newtext

stripPrefix = (str) ->
    prefixMatch = new RegExp(/(?!xmlns)^.*:/);
    return str.replace(prefixMatch, '');

xmlToJson = (file) ->
  xmlFile = fs.readFileSync file, {encoding: 'utf8'}
  parseOpts = {  attrkey: '@', tagNameProcessors: [stripPrefix], explicitArray: false }
  parser = new xml2js.Parser parseOpts
  fs.readFile './data/' + folderName + '/recordsToConvert.xml', (err, data) ->
    parser.parseString data, (err, result) ->
      if err?
        throw err
      #console.dir result
      console.log 'Done.'
      saveDocs = result.ListRecords.record
      writeFilePathName = 'data/' + folderName + '/records.json'
      for record, index in saveDocs by -1
      	if moment(record.metadata.dc.date).isValid() is true
      		console.log moment(record.metadata.dc.date).isValid, moment(record.metadata.dc.date).toDate()
      		record.metadata.dc.date = moment(record.metadata.dc.date).toDate()
      	else
      		record.metadata.dc.date = moment('1912').toDate()

      	if index is 0
		      jsonData = json.plain saveDocs
		      console.log 'docs' + saveDocs.length
		      writableJsonData = trimLines jsonData
		      insertToDb saveDocs
		      fs.writeFile './' + writeFilePathName, writableJsonData, (err) ->
		        if err?
		          throw err
		        console.log 'wrote Json file'
		        return
		      return
		  return      #writeToMongo 'Metadata', writeFilePathName
		return
		console.log 'closed'
  return

concatResponses = (folder) ->
  console.log loopIndex
  saveData = ''
  saveFile = "./data/" + folderName + "/records.xml"
  fs.readdir "./data/"+folder, (err, files) ->
    if err? 
      throw err
    for file, index in files by -1
      console.log file, index
      theIndex = index
      data = fs.readFileSync "./data/"+folder+"/"+file, {encoding: 'utf8'}
      console.log data.split('\n')[8]
      saveData = data + saveData
      if index is 0
        fs.writeFile saveFile, saveData, (err) ->
          if err?
            throw err
          else
            console.log 'Wrote Concatenated XML File'
            removeInteriorDoctypes saveFile
          return
    return
  return

makeRequest options

